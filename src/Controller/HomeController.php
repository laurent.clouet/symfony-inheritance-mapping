<?php

namespace App\Controller;

use App\Entity\Animal;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Home controller
 * @Route("")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(Request $request): Response
    {
        return new Response("index");
    }

    /**
     * @Route("/animal/{animal}", methods="GET")
     */
    public function loadAnimal(Animal $animal): Response
    {
        return new Response($animal->walk());
    }

}