<?php

namespace App\DataFixtures;

use App\Entity\Animal;
use App\Entity\Cat;
use App\Entity\Mouse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AnimalDataFixture extends Fixture {
    public function load(ObjectManager $manager) {
        if (is_null($manager->getRepository(Animal::class)->find('tom'))) {
            $tom = new Cat();
            $tom->setName("tom");
            $tom->setWeight(3200);
            $manager->persist($tom);
        }

        if (is_null($manager->getRepository(Animal::class)->find('jerry'))) {
            $jerry = new Mouse();
            $jerry->setName("jerry");
            $jerry->setWeight(50);
            $manager->persist($jerry);
        }

        $manager->flush();
    }
}