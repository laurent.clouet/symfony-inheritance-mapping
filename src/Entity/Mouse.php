<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Mouse extends Animal
{

    public function walk(): string
    {
        return "dog-walk";
    }
}