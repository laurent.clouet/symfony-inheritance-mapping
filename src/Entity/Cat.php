<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Cat extends Animal
{

    public function walk(): string
    {
        return "cat-walk";
    }
}