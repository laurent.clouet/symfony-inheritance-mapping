<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Animal
 *
 * @ORM\Table(name="animal")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\Entity(repositoryClass="App\Repository\AnimalRepository")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 */
abstract class Animal {
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected int $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    protected string $name = "";

    /**
     * @ORM\Column(name="weight", type="integer", nullable=false)
     */
    protected int $weight = 0;

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getWeight(): int {
        return $this->weight;
    }

    public function setWeight(int $weight): self {
        $this->weight = $weight;

        return $this;
    }

    abstract public function walk(): string;
}