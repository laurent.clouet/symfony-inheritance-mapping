FROM docker.phyrexia.org/docker-library/php:8.1-apache
MAINTAINER Laurent Clouet <laurent.clouet@nopnop.net>

RUN sed -i -e 's/\/var\/www\/html/\/var\/www\/html\/public/' /etc/apache2/sites-available/000-default.conf

COPY --chown=www-data:www-data . /var/www/html/

RUN runuser -s /bin/bash -c "cd /var/www/html && APP_ENV=prod composer install --no-dev --no-progress --no-suggest --optimize-autoloader" - www-data

COPY docker/entrypoint.sh /
RUN chmod 755 /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["apache2-foreground"]
